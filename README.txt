Hi Folks,

You can use this project (web application) under the "www" folder to create a new cordova project for your choice of platform.

This project uses the "cordova-plugin-device-orientation" plugin to utilize the device compass within the web application. So you have to add the "cordova-plugin-device-orientation" plugin to your cordova project.

You can use the "config.xml" file provided here in your cordova project but when you create a new project you will have your own "config.xml" that I'm encouraging you to use.

Happy coding...
Cheers