sap.ui.define([
    "com/demo/app01/controller/BaseController",
    "sap/ui/core/TitleLevel",
    "sap/m/PageBackgroundDesign",
    'sap/m/Popover',
    'sap/m/Button',
    'sap/m/MessageToast'

], function(BaseController, TitleLevel, PageBackgroundDesign, Popover, Button, MessageToast) {
    "use strict";


    var options = {
        frequency: 250
    };
    var watchID = undefined;
    var oCompassInfo = undefined;

    return BaseController.extend("com.demo.app01.controller.Home", {
        onInit: function() {
            var oShell = this.getView().byId("homeViewShell");
            oCompassInfo = this.getView().byId("compassInfo");
            oShell.setShowPane(false);
        },
        onDisplayNotFound: function(oEvent) {
            //display the "notFound" target without changing the hash
            this.getRouter().getTargets().display("notFound", {
                fromTarget: "home"
            });

        },

        onEmpGetListSelect: function(oEvent) {
            this.getRouter().navTo("employeeList");
        },
        onAccoutPress: function(oEvent) {

            var popover = new Popover({
                showHeader: false,
                placement: sap.m.PlacementType.Bottom,
                content: [
                    new Button({
                        text: 'Feedback',
                        type: sap.m.ButtonType.Transparent
                    }),
                    new Button({
                        text: 'Help',
                        type: sap.m.ButtonType.Transparent
                    }),
                    new Button({
                        text: 'Logout',
                        type: sap.m.ButtonType.Transparent
                    })
                ]
            }).addStyleClass('sapMOTAPopover sapTntToolHeaderPopover');

            //

            popover.openBy(oEvent.getSource());

        },
        onSideNavButtonPress: function(oEvent) {
            var toolPage = this.getView().byId("toolPage");
            var sideExpanded = toolPage.getSideExpanded();
            toolPage.setSideExpanded(!toolPage.getSideExpanded());
        },
        onShellMenuPress: function(oEvent) {
            var oItem = oEvent.getSource();
            var oShell = this.getView().byId("homeViewShell");
            var bState = oShell.getShowPane();
            oShell.setShowPane(!bState);
            oItem.setShowMarker(!bState);
            oItem.setSelected(!bState);
        },

        onCompassSuccess: function(heading) {
            oCompassInfo.setValue(heading.magneticHeading);
        },

        onCompassError: function(compassError) {
            oCompassInfo.setValue("");      
            MessageToast.show('Compass error: ' + compassError.code);
        },
        
        onCompassOnPress: function(oEvent) {
            watchID = navigator.compass.watchHeading(this.onCompassSuccess, this.onCompassError, options);
            MessageToast.show("Compass is ON!");
        },
        
        onCompassOffPress: function(oEvent) {
            navigator.compass.clearWatch(watchID);
            oCompassInfo.setValue("");
            MessageToast.show("Compass is OFF!");
        }

    });

});
