sap.ui.define([
	"com/demo/app01/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("com.demo.app01.controller.employees.EmployeeList", {
		onListItemPressed : function (oEvent) {
			// var sPath, oRouter;
			// sPath = oEvent.getSource().getBindingContext("northwindOData").getPath().substr(1);
			// oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			// oRouter.navTo("employeeDetail", {
			// 	employeePath: sPath
			// });

			// var oItem, oCtx, sEmployeeId;
			// oItem = oEvent.getSource();
			// oCtx = oItem.getBindingContext("northwindOData");
			// sEmployeeId = oCtx.getProperty("EmployeeID");
			// this.getRouter().navTo("employees", {
			// 	employeeId : sEmployeeId
			// });	

			var oItem = oEvent.getSource();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var sEmployeePath = oItem.getBindingContext("northwindOData").getPath().substr(1);
			console.log(sEmployeePath);
			oRouter.navTo("employees", {
				employeeId: sEmployeePath
			});

		}
	});

});