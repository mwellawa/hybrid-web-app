sap.ui.define([
    "com/demo/app01/controller/BaseController"
], function(BaseController) {
    "use strict";

    return BaseController.extend("com.demo.app01.controller.employees.EmployeeDetails", {
        onInit: function() {
            // var oRouter = this.getRouter();
            // oRouter.getRoute("employeeDetail").attachMatched(this._onRouteMatched, this);
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.getRoute("employees").attachPatternMatched(this._onObjectMatched, this);





        },
        _onObjectMatched: function (oEvent) {
            this.getView().bindElement({
                path: "/" + oEvent.getParameter("arguments").employeeId,
                model: "northwindOData",
                events: {
                    change: this._onBindingChange.bind(this),
                    dataRequested: function(oEvent) {
                        oView.setBusy(true);
                    },
                    dataReceived: function(oEvent) {
                        oView.setBusy(false);
                    }
                }
            });
        },        
        _onBindingChange: function(oEvent) {
            var oCtx = this.getView().getBindingContext("northwindOData");
            if (!oCtx) {
                this.getRouter().getTargets().display("notFound");
            } else {

                var oODataModel = this.getView().getModel("northwindOData");
                // var oCtx = this.getView().getBindingContext("northwindOData");
                var sProp = oODataModel.getOriginalProperty("Photo", oCtx);
                var oHTMLContainer = this.getView().byId("plainHtmlImage");
                sProp = sProp.substr(104);
                var sSrc = "<img src=data:image/bmp;base64," + sProp + " />";
                oHTMLContainer.setContent(sSrc);

                // debugger;
                // console.log("Path : " + oCtx.getPath());
                // var oODataModel = this.getView().getModel("northwindOData");
                // var sProp = oODataModel.getOriginalProperty("Photo", oCtx);
                // console.log("Property Value1 : " + sProp);
                // var oImageControl = this.getView().byId("EmployeeImage");
                // oImageControl.setSrc("data:image/bmp;base64, " + sProp);
                // var oBase64 = oCtx.Photo;
                // var oMimeType = "image/jpeg";
                // var oImageObject = this.getView().byId("EmployeeImage");
                // oImageObject.setAttribute("data", "data:" + oMimeType + ";base64," + oBase64);
            }
        }
    });
});